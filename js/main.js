(function () {
  var countTap = 0;
  $("[data-fire-event]")
    .on("touchstart", function () {
      countTap++;
    })
    .on("touchend", function () {
      if (countTap < 2) {
        CommunicateEmbedded.fireEvent($(this).attr("data-fire-event"));
      }
      countTap = 0;
    });
}).call(this);
CommunicateEmbedded.suppressNavigation();
CommunicateEmbedded.ready(function () {
  CommunicateEmbedded.enableNavigation();
});

